#!/bin/bash
# vim: set ft=sh:

# Setup script for ddenv.cfg.
# (c) 2016 Nilangshu Bidyanta

# This needs to be run as root since we will be copying the script to /usr/bin.

prg_name="ddenv.cfg"
print_usage()
{
	cat << EOF

Usage: setup.sh [options]

OPTIONS
-i, --install
	Install program
-u, --uninstall
	Uninstall program

EOF
		exit 1
}

check_root()
{
	if [[ $EUID != 0 ]]; then
		echo "This script must be run as root."
		exit 1
	fi
}

case "$1" in
	-i | --install)
		check_root
		cp ddenv.cfg /usr/bin/$prg_name
		echo "Installed in /usr/bin"
		;;
	-u | --uninstall)
		check_root
		if [[ -r /usr/bin/$prg_name ]]; then
			rm -f /usr/bin/$prg_name
			echo "Uninstalled $prg_name"
		else
			echo "Nothing to uninstall."
		fi
		;;
	*)
		print_usage
		;;
esac
