# Docker Development Environment #

This repository contains my attempts at dockerizing the development environments I use / have used to make them portable across systems. Currently supported are C/C++, Rust, Python with support for Kivy and C/C++ for LLVM / Clang development. While these can be used for developing and testing TUIs and command line driven programs, there is no support for GUI testing since Docker doesn't support GUIs within containers.

### Project Hierarchy ###

The project has two parts to it. The first one is `ddenv.cfg` which is a shell script used to manage Docker containers. This script is used to build and run the actual development environment images which are the second part of the project.
Each development environment comes with `vim`, `tmux`, `git` and a 256 color bash shell. The settings for these components are stored under `core-env-files/`. Any configuration file for a component shared across the Docker images is expected to be in that directory. Specific additions to these files can be made in the individual development environment directories. The modifications will be added to the template configuration files in the Makefile to build the specific environment.

### How do I get set up? ###

* Make sure `docker` and `make` are installed on the host system.

* Install the Docker container management shell script:
```
$ sudo ./setup.sh --install
```

* The container management script comes with extensive usage examples. To view them:
```
$ ddenv.cfg --examples
```

* To build the Docker images of the development environments (example for the Rust development environment):
```
$ cd ddenv-rust
$ make
```

* Containers should be run from the project directory as follows. The project files (i.e. current directory) are mounted on to the container's file system under `/prj`. The container starts in the user's home directory. To switch to the mounted project directory type in `cdw` which stands for change to work directory:
```
$ pwd
/work/my_rust_project
$ ddenv.cfg --run nb0dy/rust
Creating and running the container
Image name: nb0dy/rust
Last image timezone: America/New_York
[nb0dy:~]$ cdw
[nb0dy:/prj]$
```

### License ###

`ddenv.cfg` and the accompanying Docker Development Environment images are released under [MIT License](https://opensource.org/licenses/MIT).
