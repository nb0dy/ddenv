#!/bin/bash
# Create attachment from an ASCII file for a bash script
# The output of this can be appended to the end of ddenv.cfg and then moved
# into the correct location.
echo "read -r -d '' mfile << "EOF""
cat Makefile
echo -n "EOF"
