#!/bin/bash
# vim: set ft=sh:

# ddenv.cfg : Simple wrapper to create / save / load / run the ddenv docker image.
# (c) 2016 Nilangshu Bidyanta [nb0dy] <nbidyanta@gmail.com>

# Exit codes:
# 0 = Success
# 1 = Printed usage (non-fatal error)
# 2 = Parameter mismatch
# 3 = Fatal error
SUCCESS=0
ERR_USAGE=1
ERR_PARAM=2
ERR_FATAL=3

prg_name="ddenv.cfg"
def_usr_name="nb0dy"
unique_id="ddenv/$(pwd)"

# XXX: If persistant settings are desired, use ".ddenv" in $HOME to store the
# settings.
tmp_file_user="/tmp/$unique_id/ddenv.user"
tmp_file_image="/tmp/$unique_id/ddenv.img"
tmp_file_tz="/tmp/$unique_id/ddenv.tz"

read -r -d '' mfile << "EOF"
# Generic Makefile. Use this as a base for projects.
# This Makefile assumes the following directory structure:
#	project_home
#	├── $(BUILD_DIR)
#	├── $(INC_DIR)
#	│   ├── sub_dir_1
#	│   │      ...
#	│   └── sub_dir_n
#	└── $(SRC_DIR)
#	    ├── sub_dir_1
#	    ├── sub_dir_1
#	    │      ...
#	    └── sub_dir_m
#
# All object files are dumped into $(BUILD_DIR)/ before linking
# Please ensure that local include files (i.e. files in $(INC_DIR))
# are included with quotes (Eg. #include "module/my_header.h") instead of
# angle brackets (Eg. #include <module/my_header.h>). This is because the
# Makefile uses the GNU compatible compiler's ability to spit out dependencies
# by analyzing the source files.

CC := g++
LINKER := g++
EXEC := a.out

BUILD_DIR := build
SRC_DIR := src
INC_DIR := inc

INCFLAGS := -I $(INC_DIR)/
CXXFLAGS := -std=c++14 -Wall $(INCFLAGS)
LDFLAGS :=

SRCFILES := $(wildcard src/*.cpp src/**/*.cpp)
OBJFILES := $(patsubst %.cpp, $(BUILD_DIR)/%.o, $(notdir $(SRCFILES)))
DEPFILES := $(patsubst %.cpp, $(BUILD_DIR)/%.d, $(notdir $(SRCFILES)))

# This macro dynamically creates build rules for the object files
define create_rule
$(BUILD_DIR)/$(shell g++ -MM $(CXXFLAGS) $1)
	$$(CC) -c $$(CXXFLAGS) $$< -o $$@
endef

.PHONY: clean all

all: $(BUILD_DIR) $(BUILD_DIR)/$(EXEC)

$(BUILD_DIR):
	mkdir $(BUILD_DIR)

$(BUILD_DIR)/$(EXEC): $(OBJFILES)
	$(LINKER) $^ $(LDFLAGS) -o $@

# Rules for building object files will be instantiated here
$(foreach src_file,$(SRCFILES),$(eval $(call create_rule,$(src_file))))

clean:
	rm -rf $(BUILD_DIR)
EOF

# Print the usage of this script to the user
print_usage()
{
	cat << EOF

$prg_name: Docker based Development ENVironment.
(c) 2016 Nilangshu Bidyanta

Usage: $prg_name [options]

OPTIONS
-b, --build <image name> [username]
	Build the image from the local Dockerfile.
	Default username is 'nb0dy'. In absence of parameters, use settings
	from the last build.

-s, --save <image name>
	Save the image into a compressed tar archive.

-l, --load <archive name>
	Load the image from a compressed tar archive.

-r, --run <image name>
	Run a container from the most recently built image or the one supplied."

-c, --clean
	Clean unreferenced images from the cache.

-ec, --erase-cred
	Erase stored credentials.

-et, --erase-tz
	Erase stored timezone setting.

-cp, --create-prj <project name> <c|cxx>
	Create an empty C/C++ project directory structure.

-x, --examples
	Show examples for the commands.

EOF
	exit $ERR_USAGE
}

# Print the error message and exit with the given error code
print_error()
{
	if [[ $# != 2 ]]; then
		echo "Fatal error occured."
		exit $ERR_FATAL
	fi
	echo "Error: "$1""
	exit $2
}

# Create an empty file if it doesn't exist along with the directory structure
create_file()
{
	if [[ $# != 1 ]]; then
		return;
	fi
	filename=$1
	if [[ -r $filename ]]; then
		return;
	fi
	directories=$(dirname $filename)
	mkdir -p $directories
	touch $filename
}

# Instruct docker to build an image from the Dockerfile. If the name of the
# image is not provided, load it and the username from a prior build, if
# possible. If the user name isn't provided, fall back to a default one.
build_img()
{
	echo "Building image"
	num_param=$#
	img_name=''
	username=''
	loaded="false"
	if (( $num_param >= 4 )); then
		print_error "'--build' accepts 2 parameters." $ERR_PARAM
	elif (( $num_param < 2 )); then
		if [[ -r $tmp_file_image ]]; then
			read -e img_name < "$tmp_file_image"
			echo "Last image name: $img_name"
		fi
		if [[ -r $tmp_file_user ]]; then
			read -e username < "$tmp_file_user"
			echo "Last username: $username"
		fi
		if [[ -z $img_name ]]; then
			print_error "'--build' needs an image name." $ERR_PARAM
		fi
		loaded="true"
	else
		img_name="${@:2:1}"
		username="${@:3:1}"
	fi

	if [[ -z $username ]]; then
		username=$def_usr_name
	fi
	if [[ $loaded == "false" ]]; then
		echo "Image name: $img_name"
		echo "Username: $username"
	fi
	docker build -t $img_name --build-arg DDENV_USER=$username .
	ret_val=$?
	if [[ $ret_val == 0 ]]; then	# Successful docker build
		create_file $tmp_file_image
		create_file $tmp_file_user
		echo $img_name > $tmp_file_image
		echo $username > $tmp_file_user
	fi
}

# Run the container based on the image name provided. If no image name is
# provided, attempt loading the name from a prior build.
run_cnt()
{
	echo "Creating and running the container"
	num_param=$#
	img_name=''
	tz_data=''
	loaded="false"
	if (( $num_param >= 3)); then
		print_error "'--run' accepts 1 parameter." $ERR_PARAM
	elif (( $num_param <= 1 )); then
		if [[ -r $tmp_file_image ]]; then
			read -e img_name < "$tmp_file_image"
			echo "Last image name: $img_name"
		fi
		loaded="true"
		if [[ -z $img_name ]]; then
			print_error "'--run' needs an image name." $ERR_PARAM
		fi
	else
		img_name="${@:2:1}"
	fi
	if [[ $loaded == "false" ]]; then
		echo "Image name: $img_name"
	fi
	machine_name=$(echo $img_name | cut -d : -f 1 | tr '\\/' '-')

	# Set the local timezone of the image once. Delete the file $tmp_file_tz
	# if you wish to re-enter the timezone.
	if [[ -r $tmp_file_tz ]]; then
		read -e tz_data < "$tmp_file_tz"
		echo "Last image timezone: $tz_data"
	else
		echo -n "Please enter the timezone [America/New_York]: "
		read tz_data
		if [[ -z $tz_data || ! -e /usr/share/zoneinfo/$tz_data ]]; then
			tz_data="America/New_York"
		fi
		create_file $tmp_file_tz
		echo $tz_data > $tmp_file_tz
	fi

	# If running through docker-machine, this command needs to be issued
	# after ssh-ing into the VM:
	# docker-machine ssh <machine-name> -A
	container_name=$(basename $(pwd))
	docker run --name $container_name -h $machine_name -e "TZ=$tz_data" \
		-v $(dirname $SSH_AUTH_SOCK):$(dirname $SSH_AUTH_SOCK) \
		-v $(pwd):/prj -e SSH_AUTH_SOCK=$SSH_AUTH_SOCK -it --rm $img_name

	ret_val=$?
	if [[ $ret_val == 0 ]]; then	# Successful docker run
		create_file $tmp_file_image
		echo $img_name > $tmp_file_image
	fi
}

# Save the image to a XZ compressed tar archive.
save_img()
{
	echo "Saving image"
	if [[ $# != 2 ]]; then
		print_error "The name of the image is needed." $ERR_PARAM
	fi
	img_name="$2"
	archive_name=$(echo $img_name | tr '\\/:' '-')
	docker save -o /tmp/$archive_name.tar $img_name
	echo "Compressing image"
	xz -kv --threads=0 /tmp/$archive_name.tar
	mv /tmp/$archive_name.tar.xz .
	echo "Removing temporary files"
	rm /tmp/$archive_name.tar
	echo "Image saved as: $archive_name.tar.xz"
}

# Load the image from a saved XZ compressed tar archive.
load_img()
{
	if [[ $# != 2 ]]; then
		print_error "The name of the archive is needed." $ERR_PARAM
	fi
	archive_name="$2"
	tarball_name=$(basename $archive_name .xz)
	echo "Decompressing image"
	xz -kvd --threads=0 --stdout $archive_name > /tmp/$tarball_name
	echo "Loading image"
	docker load -i /tmp/$tarball_name
	echo "Removing temporary files"
	rm /tmp/$tarball_name
	echo "Archive $archive_name loaded as a Docker image"
}

# Delete stored user credential and image name.
delete_cred()
{
	rm -f $tmp_file_image
	rm -f $tmp_file_user
	echo "Stored credentials have been erased."
}

# Delete stored timezone information.
delete_tz()
{
	rm -f $tmp_file_tz
	echo "Stored timezone info has been erased."
}

# Print examples on the screen.
show_examples()
{
	bold=$(tput bold)
	nrml=$(tput sgr0)
	unbg=$(tput smul)
	unen=$(tput rmul)
	less -r << EOF
${unbg}Create Project example${unen}:

${bold}ddenv.cfg -cp my_proj c${nrml}
${bold}ddenv.cfg -cp my_proj cxx${nrml}

The above command creates an empty C or C++ project and supplies a generic
Makefile (that you may need to modify). The Makefile only works with the directory
structure described inside it and requires a make version >= 3.81. Ideally, this
command should be invoked from your project home where you'd like to store the
project.


For the following examples, this program must be invoked from the root of the
project directory. That will ensure the entire project tree is loaded into the
container. Moreover, multiple project environment containers can be run,
assuming each project working directory has a unique name.

${unbg}Build example${unen}:

${bold}ddenv.cfg -b nb0dy/ddenv:v0${nrml}
${bold}ddenv.cfg -b${nrml}

The name of the image is expected to be in the regular Docker 'name:tag' format.
Once an image has been built, the program can be invoked without specifying an
image name. Names stored are not persistant across system restarts. They are
unique to the location in the filesystem where they are invoked. For example,
ddenv.cfg can be invoked at '/work/prj1' and '/work/prj2' without the image name
after successful builds at each location.


${unbg}Run example${unen}:

${bold}ddenv.cfg -r nb0dy/ddenv:v0${nrml}
${bold}ddenv.cfg -r${nrml}

Similar to the 'build' option, 'run' expects the image name for the very first
time; names are not persistant across system restarts. After a successful build
or a previous successful run, the image name becomes optional. The container
name is borrowed from the current working directory.


${unbg}Save and load examples${unen}:

${bold}ddenv.cfg -s nb0dy/ddenv:v0${nrml}
${bold}ddenv.cfg -l nb0dy-ddenv-v0.tar.xz${nrml}

Saving an image creates a XZ compressed tar archive. Slashes and colons are
replaced by hyphens. Use the generated file as the parameter to load to load the
image.


${unbg}Other examples${unen}:

${bold}ddenv.cfg --clean${nrml}
${bold}ddenv.cfg --erase-cred${nrml}
${bold}ddenv.cfg --erase-tz${nrml}
${bold}ddenv --examples${nrml}

The first command cleans any unreferenced (dangling) image references. The second
removes any stored image name associated with the current location in the file-
system. The third command clears any stored timezones. The fourth shows this
help.

EOF
}

# Get rid of any dangling (unreferenced) images. Helps free up diskspace.
clean_image_cache()
{
	img_list=$(docker images -f "dangling=true" -q)
	if [[ -n $img_list ]]; then
		docker rmi $img_list
		echo "Cleaned unreferenced images."
		exit $SUCCESS
	fi
	echo "No unreferenced images found."
}

# Create the empty directory structure along with the Makefile for a C/C++
# project. The default compiler used is the GNU C/C++ compiler.
create_prj()
{
	num_param=$#
	if (( $num_param != 3 )); then
		print_error "'--create-prj' needs 2 parameters." $ERR_PARAM
	fi

	if [[ -d $2 ]]; then
		print_error "Directory '$2' already exists." $ERR_PARAM
	fi

	if [[ "$3" != "c" && "$3" != "C" && "$3" != "cxx" && "$3" != "CXX" ]]; then
		print_error "Project type must be either 'c' or 'cxx'." $ERR_PARAM
	fi

	# Create the directory structure outlined in the Makefile.
	# Ensure Makefile is C compatible if that option was selected.
	mkdir $2
	if [[ "$3" == 'c' || "$3" == 'C' ]]; then
		echo "$mfile" | sed -e "s/a\.out/$2/" \
			-e "s/g++/gcc/" \
			-e "s/\.cpp/\.c/g" \
			-e "s/-std=c++14//" \
			-e "s/CXXFLAGS/CFLAGS/g" > "$2"/Makefile
	else
		echo "$mfile" | sed -e "s/a\.out/$2/" > "$2"/Makefile
	fi
	mkdir "$2"/src
	mkdir "$2"/inc

	# A tiny helper script to open up the vim workspace
	cat << "EOF" > "$2"/workspace
#!/bin/bash
vim $(find src/ inc/ -type f -name "*c*" -o -name "*h")
EOF
	chmod +x "$2"/workspace
}


# Entry point of the script.
case "$1" in
	-b | --build)
		build_img "$@"
		;;
	-s | --save)
		save_img "$@"
		;;
	-l | --load)
		load_img "$@"
		;;
	-r | --run)
		run_cnt "$@"
		;;
	-c | --clean)
		clean_image_cache
		;;
	-ec | --erase-cred)
		delete_cred
		;;
	-et | --erase-tz)
		delete_tz
		;;
	-cp | --create-prj)
		create_prj "$@"
		;;
	-x | --examples)
		show_examples
		;;
	*)
		if [[ -n "$1" ]]; then
			echo "Unkown option '$1'"
		fi
		print_usage
		;;
esac
