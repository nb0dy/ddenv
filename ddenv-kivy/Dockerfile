FROM ubuntu:14.04
MAINTAINER Nilangshu

# Install core utilities needed
RUN DEBIAN_FRONTEND=noninteractive apt-get -yq --no-install-recommends update && \
	DEBIAN_FRONTEND=noninteractive apt-get -yq --no-install-recommends install \
	software-properties-common && \
	DEBIAN_FRONTEND=noninteractive add-apt-repository -y ppa:ubuntu-toolchain-r/test && \
	DEBIAN_FRONTEND=noninteractive apt-get -yq --no-install-recommends update && \
	DEBIAN_FRONTEND=noninteractive apt-get -yq --no-install-recommends install \
	gcc-5 \
	g++-5 \
	gdb \
	make \
	openssh-client vim git tmux curl man-db tree wget astyle && \
	update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 10 && \
	update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-5 10

# Install Python 2.7 and Kivy support. CMake and python dev is needed for YouCompleteMe
RUN DEBIAN_FRONTEND=noninteractive add-apt-repository -y ppa:kivy-team/kivy && \
	DEBIAN_FRONTEND=noninteractive apt-get -yq --no-install-recommends update && \
	DEBIAN_FRONTEND=noninteractive apt-get -yq --no-install-recommends install \
	python2.7 \
	python-dev \
	python-pip \
	python-kivy && \
	pip install flake8 jedi

# Set the user name for the development environment here
ARG DDENV_USER=nb0dy

# Add user and set up the home directory
RUN useradd -m -d /home/$DDENV_USER $DDENV_USER && \
	chown -R $DDENV_USER: /home/$DDENV_USER

# Set the user that will be running the container and the working directory
USER $DDENV_USER
WORKDIR /home/$DDENV_USER

# Set up VIM
# -- Set up Pathogen and some basic plugins. These include:
# 	o Pathogen for easy addition of plugins
# 	o Ctrl+P
# 	o A
# 	o STL Syntax plugin
# 	o Solarized Dark theme
# 	o Multiple cursor plugin
# 	o Vim surround
# 	o Vim repeat
# 	o Jedi (Python semantic completion)
# 	o Syntastic
RUN mkdir -p /home/$DDENV_USER/.vim/autoload /home/$DDENV_USER/.vim/bundle && \
	curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim && \
	git clone https://github.com/ctrlpvim/ctrlp.vim.git /home/$DDENV_USER/.vim/bundle/ctrlp.vim && \
	git clone https://github.com/vim-scripts/a.vim.git /home/$DDENV_USER/.vim/bundle/a.vim && \
	git clone https://github.com/octol/vim-cpp-enhanced-highlight.git /home/$DDENV_USER/.vim/bundle/vim-cpp-enhanced-highlight && \
	git clone git://github.com/altercation/vim-colors-solarized.git /home/$DDENV_USER/.vim/bundle/vim-colors-solarized && \
	git clone https://github.com/tpope/vim-surround /home/$DDENV_USER/.vim/bundle/vim-surround && \
	git clone https://github.com/terryma/vim-multiple-cursors /home/$DDENV_USER/.vim/bundle/vim-multiple-cursors && \
	git clone https://github.com/tpope/vim-repeat /home/$DDENV_USER/.vim/bundle/vim-repeat && \
	git clone --recursive https://github.com/davidhalter/jedi-vim.git /home/$DDENV_USER/.vim/bundle/jedi && \
	git clone https://github.com/scrooloose/syntastic.git /home/$DDENV_USER/.vim/bundle/syntastic

# Clean-up apt and support programs installed
USER root
RUN apt-get clean autoclean && \
	apt-get autoremove -y && \
	update-alternatives --remove gcc /usr/bin/gcc-5 && \
	update-alternatives --remove g++ /usr/bin/g++-5 && \
	apt-get remove -y gcc-5 g++-5 gdb && \
	rm -rf /var/lib/apt /var/lib/dpkg /var/lib/cache /var/lib/log /var/tmp

# Switch back to environment user
USER $DDENV_USER

# Set up the shell and editor environments (target is working directory)
ADD bashrc .bashrc
ADD vimrc .vimrc
ADD tmux.conf .tmux.conf
ADD dircolors.256dark .dircolors.256dark
ADD gitconfig .gitconfig

# Set the locale to UTF-8. Otherwise, Vim won't display unicode characters properly.
ENV LANG C.UTF-8

# Ensure SHELL points to Bash. Tmux uses this to spawn new shells. Default is /bin/sh.
ENV SHELL /bin/bash

# Start the Bash shell
CMD /bin/bash
