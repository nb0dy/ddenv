FROM nb0dy/ddenv
MAINTAINER Nilangshu

# Set the user name for the development environment here
ARG DDENV_USER=nb0dy

# The following commands must be run as root
USER root

# Install from source CMake (3.5.2), LLVM & Clang (3.7.0)
RUN echo "Current directory: $(pwd)" && \
	wget --progress=bar:force \
	http://llvm.org/releases/3.7.0/llvm-3.7.0.src.tar.xz \
	http://llvm.org/releases/3.7.0/cfe-3.7.0.src.tar.xz \
	https://cmake.org/files/v3.5/cmake-3.5.2.tar.gz && \
	xz -vd llvm-3.7.0.src.tar.xz && \
	tar zxvf cmake-3.5.2.tar.gz && \
	rm cmake-3.5.2.tar.gz && \
	tar xf llvm-3.7.0.src.tar && \
	rm llvm-3.7.0.src.tar && \
	mv cfe-3.7.0.src.tar.xz llvm-3.7.0.src/tools/ && \
	cd llvm-3.7.0.src/tools/ && \
	xz -vd cfe-3.7.0.src.tar.xz && \
	tar xf cfe-3.7.0.src.tar && \
	rm cfe-3.7.0.src.tar && \
	cd ../..

# Compile CMake and delete the source
RUN cd cmake-3.5.2/ && \
	./bootstrap && make && make install && \
	cd .. && rm -rf cmake-3.5.2/

# Compile and install LLVM + Clang libraries.
# If on a multiprocessor / multicore system, use "make -j$(nproc)"
# instead of "make".
RUN cd llvm-3.7.0.src/ && \
mkdir build && cd build && \
cmake -G "Unix Makefiles" \
	-DCMAKE_C_COMPILER=/usr/bin/gcc \
	-DCMAKE_CXX_COMPILER=/usr/bin/g++ \
	-DCMAKE_BUILD_TYPE=Release \
	-DLLVM_TARGETS_TO_BUILD=X86 .. && \
	make -j2 && \
	make install && \
	cd ../.. && rm -rf llvm-3.7.0.src/

# Set the user that will be running the container and the working directory
USER $DDENV_USER
